<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Requests\Brand\StoreRequest;
use Illuminate\Http\Request;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $brands = Brand::all();
            return response()->json([
                'brand' =>$brands,
                'status' => 'success'
            ],200);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'failure'
            ],500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
            $brand = Brand::create($request->all());
            return response()->json([
                $brand,
                'status' => 'success'
            ],200);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'failure'
            ],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        try {
            return response()->json([
                'data'=> $brand,
                'status' => 'success'
            ],200);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'failure'
            ],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, Brand $brand)
    {
        try {
            $brand->update($request->all());
            $brand->save();
            return response()->json([
                $brand,
                'status' => 'success'
            ],200);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'failure'
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        try {
            $brand->delete();
            return response()->json([
                $brand,
                'status' => 'success'
            ],200);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'failure'
            ],500);
        }
    }
}
