<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Requests\Car\StoreRequest;
use Illuminate\Http\Request;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $cars = Car::with('brand')->get();
            return response()->json([
                'cars' =>$cars,
                'status' => 'success'
            ],200);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'failure'
            ],500);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
            $car = new Car();
            $car->name      = $request->name;
            $car->brand_id  = $request->brand_id;

            $car->save();

            return response()->json([
                'cars' =>$car,
                'status' => 'success'
            ],200);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'failure'
            ],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        try {
            return response()->json([
                'cars' =>$car,
                'status' => 'success'
            ],200);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'failure'
            ],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, Car $car)
    {
        try {
            $car->name      = $request->name;
            $car->brand_id  = $request->brand_id;

            $car->save();

            return response()->json([
                'cars' =>$car,
                'status' => 'success'
            ],200);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'failure'
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        try {

            $car->delete();

            return response()->json([
                'cars' =>$car,
                'status' => 'success'
            ],200);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json([
                'message' => 'Contact the Administrator',
                'status' => 'failure'
            ],500);
        }
    }
}
