<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' =>$validator->errors(),
            ],400);
        }

        try {
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt( $request->password );

            $user->save();

            return response()->json([
                'data' => $user,
                'message' => 'success'
            ],200);
        } catch (\Exception $th) {
            Log::error($th->getMessage());
            return response()->json([
                'message' => 'Something went wrong contact the administrator',
                'status' => 'error'
            ],500);
        }
    }

    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        try {
            $http = new \GuzzleHttp\Client;

            $response = $http->post(route('passport.token'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => env('CLIENT_ID'),
                    'client_secret' => env('CLIENT_SECRET'),
                    'username' => $request->email,
                    'password' => $request->password,
                    'scope' => '*',
                ],
            ]);

            $data = $response->getBody();

            return  response($data, 200);

        } catch (GuzzleHttp\Exception\BadResponseException $e) {
            if ($e->getCode() === 400) {
                Log::error($e->getMessage());
                return response()->json(['message' => 'Please enter email and password.'], $e->getCode());
            } else if ($e->getCode() === 401) {
                Log::error($e->getMessage());
                return response()->json(['message' => 'Your credentials are incorrect. Please try again'], $e->getCode());
            }
            Log::error($e->getMessage());
            return response()->json(['message' => 'Something is bad, contact with the administrator'], $e->getCode());
        }


    }

    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json('Success exit', 200);
    }
}
