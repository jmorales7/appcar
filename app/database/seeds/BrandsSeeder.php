<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            [
                'name' => 'AUDI'
            ],
            [
                'name' => 'BMW'
            ],
            [
                'name' => 'HONDA'
            ],
            [
                'name' => 'NISSAN'
            ],
            [
                'name' => 'KIA'
            ],
            [
                'name' => 'SEAT'
            ],
            [
                'name' => 'VW'
            ],
        ]);
    }
}
