import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BrandsService {
  private url = environment.app_url

  constructor(private http: HttpClient) { }

  getBrands() {
    return this.http.get<any>(this.url + '/brands')
  }

  getBrand (id) {
    return this.http.get<any>(this.url + '/brands/'+id)
  }

  createBrand(brand) {
    return this.http.post<any>(this.url + '/brands',brand)
  }

  updateBrand(brand) {
    return this.http.put<any>(this.url + '/brands/'+ brand.id,brand)
  }

  deleteBrand(brand) {
    return this.http.delete<any>(this.url + '/brands/'+ brand.id)
  }
}
