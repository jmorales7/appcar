import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import {Router} from '@angular/router'
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = environment.app_url

  constructor( private http: HttpClient, private router: Router) { }

  signUpUser (user) {
    return this.http.post<any>(this.url+'/register',user)
  }

  signInUser (user) {
    return this.http.post<any>(this.url+'/login',user)
  }

  logged() {
    return !!localStorage.getItem('token')
  }

  getToken() {
    return localStorage.getItem('token')
  }

  logout() {
    localStorage.removeItem('token')
    this.router.navigate(['signin'])
  }

}
