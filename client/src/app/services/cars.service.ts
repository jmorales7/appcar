import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CarsService {
  private url = environment.app_url

  constructor(private http: HttpClient) { }

  getCars() {
    return this.http.get<any>(this.url + '/cars')
  }

  getCar (id) {
    return this.http.get<any>(this.url + '/cars/'+id)
  }

  createCar(car) {
    return this.http.post<any>(this.url + '/cars',car)
  }

  updateCar(car) {
    return this.http.put<any>(this.url + '/cars/'+ car.id,car)
  }

  deleteCar(car) {
    return this.http.delete<any>(this.url + '/cars/'+ car.id)
  }
}
