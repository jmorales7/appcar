import { Component, OnInit } from '@angular/core';
import {BrandsService} from '../../services/brands.service'
import {Router} from '@angular/router'


@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {

  brands = []

  constructor(private brandsService: BrandsService, private router: Router) { }

  ngOnInit(): void {
    this.brandsService.getBrands()
    .subscribe(res => {
      this.brands = res.brand
    },err => {
      console.log(err)
    })
  }

  editBrand(brand) {
    localStorage.removeItem('brandId')
    localStorage.setItem('brandId',brand.id)
    this.router.navigate(['brands/edit']);
  }
  deleteBrand(brand){
    this.brandsService.deleteBrand(brand)
      .subscribe( data => {
        this.brands = this.brands.filter(b => b !== brand);
      },err => {
        alert(err)
      })
  };




}
