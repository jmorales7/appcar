import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service'
import {Router} from '@angular/router'
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  user = {
    name: '',
    password: '',
    email: ''
  }

  constructor(private authService: AuthService,private alertService: AlertService, private router: Router) { }

  ngOnInit(): void {
  }

  signup() {
    this.authService.signUpUser(this.user)
    .subscribe(res => {
      this.alertService.success('Success');
      setTimeout(()=> {
        this.router.navigate(['/signin'])

      },1500)
    },err => {
      for (const property in err.error.errors) {
        this.alertService.danger(err.error.errors[property]);
      }
      console.log(err.error)
    })
  }

}
