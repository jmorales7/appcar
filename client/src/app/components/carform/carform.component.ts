import { Component, OnInit } from '@angular/core';
import { CarsService } from '../../services/cars.service'
import { BrandsService } from '../../services/brands.service'
import { Router } from "@angular/router"
import { AlertService } from "ngx-alerts"

@Component({
  selector: 'app-carform',
  templateUrl: './carform.component.html',
  styleUrls: ['./carform.component.css']
})
export class CarformComponent implements OnInit {

  car = {
    name: '',
    brand_id: ''
  }
  brands = []

  constructor(private carService: CarsService, private brandService: BrandsService, private alertService:AlertService,private router: Router) { }

  ngOnInit(): void {
    this.brandService.getBrands()
    .subscribe(res => {
      this.brands = res.brand
    },err => {
      console.log(err)
    })
  }
  onChange(id) {
    this.car.brand_id = id
}
  create() {
    this.carService.createCar(this.car)
    .subscribe(res => {
      this.alertService.success('Success');
      setTimeout(()=> {
        this.router.navigate(['/cars'])
      },1500)
    },err => {
      for (const property in err.error.errors) {
        this.alertService.danger(err.error.errors[property]);
      }
    })
  }
}
