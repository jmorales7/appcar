import { Component, OnInit } from '@angular/core';
import { BrandsService } from '../../services/brands.service'
import { Router } from "@angular/router"
import { AlertService } from "ngx-alerts"


@Component({
  selector: 'app-formbrand',
  templateUrl: './formbrand.component.html',
  styleUrls: ['./formbrand.component.css']
})
export class FormbrandComponent implements OnInit {

  brand = {
    name: ''
  }
  constructor(private brandService: BrandsService, private alertService:AlertService,private router: Router) { }

  ngOnInit(): void {
  }

  create() {
    this.brandService.createBrand(this.brand)
    .subscribe(res => {
      this.alertService.success('Success');
      setTimeout(()=> {
        this.router.navigate(['/brands'])
      },1500)
    },err => {
      for (const property in err.error.errors) {
        this.alertService.danger(err.error.errors[property]);
      }
    })
  }

}
