import { Component, OnInit } from '@angular/core';
import {CarsService} from '../../services/cars.service'
import {Router} from '@angular/router'
import { AlertService } from "ngx-alerts"


@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  cars = []

  constructor(private carService: CarsService,private alertService:AlertService, private router: Router) { }

  ngOnInit(): void {
    this.carService.getCars()
    .subscribe(res => {
      this.cars = res.cars
    },err => {
      console.log(err)
    })
  }

  editCar(car) {
    localStorage.removeItem('carId')
    localStorage.setItem('carId',car.id)
    this.router.navigate(['cars/edit']);
  }
  deleteCar(car){
    this.carService.deleteCar(car)
      .subscribe( data => {
        this.cars = this.cars.filter(c => c !== car);
      },err => {
        console.log(err)
      })
  };
}
