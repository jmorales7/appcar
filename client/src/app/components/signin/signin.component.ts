import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service'
import {Router} from '@angular/router'
import { AlertService } from "ngx-alerts"
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  user = {
    email: '',
    password: ''
  }

  constructor(private authService: AuthService, private router: Router , private alertService:AlertService) { }

  ngOnInit(): void {
  }

  signIn () {
    this.authService.signInUser(this.user)
    .subscribe(res => {
      this.alertService.success('Success');
      localStorage.setItem('token',res.access_token)
      setTimeout(()=> {
        this.router.navigate(['/brands'])
      },1250)
    },err => {
      for (const property in err.error) {
        this.alertService.danger(err.error[property]);
      }
    })
  }


}
