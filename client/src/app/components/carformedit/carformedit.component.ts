import { Component, OnInit } from '@angular/core';
import { CarsService } from '../../services/cars.service'
import { BrandsService } from '../../services/brands.service'
import { Router } from "@angular/router"
import { AlertService } from "ngx-alerts"


@Component({
  selector: 'app-carformedit',
  templateUrl: './carformedit.component.html',
  styleUrls: ['./carformedit.component.css']
})
export class CarformeditComponent implements OnInit {


  car = {
    name: '',
    brand_id: ''
  }
  id: {}
  brands = []

  constructor(private carService: CarsService, private brandService: BrandsService,private alertService:AlertService, private router: Router) { }

  ngOnInit(): void {
    this.brandService.getBrands()
    .subscribe(res => {
      this.brands = res.brand
    },err => {
      console.log(err)
    })

    this.id = localStorage.getItem('carId')
    this.carService.getCar(this.id)
    .subscribe(res => {
      this.car = res.cars
    },err => {
      console.log(err)
    })
  }
  onChange(id) {
    this.car.brand_id = id
}

  update() {
    this.carService.updateCar(this.car)
    .subscribe(res => {
      this.alertService.success('Success');
      setTimeout(()=> {
        this.router.navigate(['/cars'])
      },1500)
    },err => {
      for (const property in err.error.errors) {
        this.alertService.danger(err.error.errors[property]);
      }
    })
  }

}
