import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarformeditComponent } from './carformedit.component';

describe('CarformeditComponent', () => {
  let component: CarformeditComponent;
  let fixture: ComponentFixture<CarformeditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarformeditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarformeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
