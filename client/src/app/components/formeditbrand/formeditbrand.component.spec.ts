import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormeditbrandComponent } from './formeditbrand.component';

describe('FormeditbrandComponent', () => {
  let component: FormeditbrandComponent;
  let fixture: ComponentFixture<FormeditbrandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormeditbrandComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormeditbrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
