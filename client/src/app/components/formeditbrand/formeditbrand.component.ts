import { Component, OnInit } from '@angular/core';
import {BrandsService} from '../../services/brands.service'
import {Router} from '@angular/router'
import { AlertService } from "ngx-alerts"

@Component({
  selector: 'app-formeditbrand',
  templateUrl: './formeditbrand.component.html',
  styleUrls: ['./formeditbrand.component.css']
})
export class FormeditbrandComponent implements OnInit {
  brand: any = {}
  id: {}
  constructor(private brandService: BrandsService,private alertService:AlertService, private router: Router) { }

  ngOnInit(): void {
    this.id = localStorage.getItem('brandId')
    this.brandService.getBrand(this.id)
    .subscribe(res => {
      this.brand = res.data
    },err => {
      console.log(err)
    })

  }

  update() {
    this.brandService.updateBrand(this.brand)
    .subscribe(res => {
      this.alertService.success('Success');
      setTimeout(()=> {
        this.router.navigate(['/brands'])
      },1500)
    },err => {
      for (const property in err.error.errors) {
        this.alertService.danger(err.error.errors[property]);
      }
    })
  }

}
