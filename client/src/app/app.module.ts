import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms"
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { AlertModule } from 'ngx-alerts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SigninComponent } from './components/signin/signin.component';
import { BrandComponent } from './components/brand/brand.component';
import { CarComponent } from './components/car/car.component';
import { SignupComponent } from './components/signup/signup.component';
import {AuthGuard} from './auth.guard'
import {TokenService} from './services/token.service';
import { FormbrandComponent } from './components/formbrand/formbrand.component';
import { FormeditbrandComponent } from './components/formeditbrand/formeditbrand.component';
import { CarformComponent } from './components/carform/carform.component';
import { CarformeditComponent } from './components/carformedit/carformedit.component'

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    BrandComponent,
    CarComponent,
    SignupComponent,
    FormbrandComponent,
    FormeditbrandComponent,
    CarformComponent,
    CarformeditComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AlertModule.forRoot({maxMessages: 5, timeout: 5000, positionX: 'right', positionY: 'top'})
  ],
  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
