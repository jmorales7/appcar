import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BrandComponent } from './components/brand/brand.component'
import { FormbrandComponent } from './components/formbrand/formbrand.component'
import { FormeditbrandComponent } from './components/formeditbrand/formeditbrand.component'


import { SigninComponent } from './components/signin/signin.component'
import { SignupComponent } from './components/signup/signup.component'

import { CarComponent } from './components/car/car.component'
import { CarformComponent } from './components/carform/carform.component'
import { CarformeditComponent } from './components/carformedit/carformedit.component'


import {AuthGuard} from './auth.guard'

const routes: Routes = [
  {
    path: '',
    redirectTo: '/brands',
    pathMatch: 'full'
  },
  {
    path: 'brands',
    component: BrandComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'brands/create',
    component: FormbrandComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'brands/edit',
    component: FormeditbrandComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'cars',
    component: CarComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'cars/create',
    component: CarformComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'cars/edit',
    component: CarformeditComponent,
    canActivate : [AuthGuard]
  },
  {
    path: 'signin',
    component: SigninComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
